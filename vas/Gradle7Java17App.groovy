
def call{
    pipeline { 
        agent any 
        stages {
            stage('Gradle Build') { 
                tools {
                    jdk 'JDK17'
                }
                steps {
                    echo 'reuse pipeline'
                    sh 'chmod +x gradlew' 
                    sh './gradlew build' 
                }
            } 
        }
        
    }
}


